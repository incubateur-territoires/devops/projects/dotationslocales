# tflint-ignore: terraform_unused_declarations
data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.scaleway_production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

# tflint-ignore: terraform_unused_declarations
data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.scaleway_development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

# tflint-ignore: terraform_unused_declarations
data "gitlab_project" "backend" {
  id = 37739253
}
# tflint-ignore: terraform_unused_declarations
data "gitlab_project" "frontend" {
  id = 37582758
}
# tflint-ignore: terraform_unused_declarations
data "gitlab_project" "data" {
  id = 37581269
}
