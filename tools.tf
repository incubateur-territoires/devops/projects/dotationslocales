resource "scaleway_object_bucket" "metabase_backups" {
  provider = scaleway.scaleway_project
  name     = "metabase-${var.project_slug}-backups"
}
module "metabase" {
  source       = "./tools/metabase"
  project_slug = "dotations"
  kubeconfig   = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  hostname     = "metabase.${var.prod_base-domain}"

  scaleway_project_id = var.scaleway_project_config.project_id
  scaleway_access_key = var.scaleway_project_config.access_key
  scaleway_secret_key = var.scaleway_project_config.secret_key
  backup_bucket_name  = scaleway_object_bucket.metabase_backups.name

  monitoring_org_id = random_string.production_secret_org_id.result

  smtp_host         = var.metabase_smtp_host
  smtp_port         = var.metabase_smtp_port
  smtp_user         = var.metabase_smtp_user
  smtp_password     = var.metabase_smtp_password
  smtp_security     = "tls"
  smtp_from_address = var.metabase_smtp_user
  smtp_from_name    = "Metabase Dotations Locales"
}
