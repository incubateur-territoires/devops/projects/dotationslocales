variable "base-domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}

variable "namespace" {
  type = string
}

variable "repositories" {
  type = list(number)
}

variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}

variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}
