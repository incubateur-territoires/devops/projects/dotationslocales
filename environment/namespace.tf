module "namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.3.5"
  namespace    = var.namespace
  max_cpu      = var.namespace_quota_max_cpu
  max_memory   = var.namespace_quota_max_memory
  project_name = var.project_name
  project_slug = var.project_slug

  quota_container_default = {
    cpu    = "50m"
    memory = "128Mi"
  }
  quota_container_default_request = {
    cpu    = "25m"
    memory = "64Mi"
  }
}
