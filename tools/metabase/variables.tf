variable "hostname" {
  type = string
}

variable "project_slug" {
  type = string
}

# variable "scaleway_organization_id" {
#   type = string
# }
variable "scaleway_project_id" {
  type = string
}
variable "scaleway_access_key" {
  type = string
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}
variable "backup_bucket_name" {
  type = string
}

variable "monitoring_org_id" {
  type      = string
  sensitive = true
}

variable "smtp_host" {
  type = string
}
variable "smtp_port" {
  type = number
}
variable "smtp_user" {
  type = string
}
variable "smtp_password" {
  type      = string
  sensitive = true
}
variable "smtp_security" {
  type = string
}
variable "smtp_from_address" {
  type = string
}
variable "smtp_from_name" {
  type = string
}
