module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  namespace         = "${var.project_slug}-metabase"
  max_cpu_requests  = "11"
  max_memory_limits = "15Gi"
  project_name      = "Metabase"
  project_slug      = "metabase"

  default_container_cpu_requests  = "10m"
  default_container_memory_limits = "16Mi"
}

module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name = "postgresql"
  namespace  = module.namespace.namespace

  pg_replicas = 1

  pg_volume_size = "5Gi"

  pg_backups_volume_enabled             = true
  pg_backups_volume_size                = "30Gi"
  pg_backups_volume_full_schedule       = "15 4 * * 0"
  pg_backups_volume_incr_schedule       = "15 4 * * 1-6"
  pg_backups_volume_full_retention      = 4
  pg_backups_volume_full_retention_type = "count"

  pg_backups_s3_enabled       = true
  pg_backups_s3_bucket        = var.backup_bucket_name
  pg_backups_s3_region        = "fr-par"
  pg_backups_s3_endpoint      = "s3.fr-par.scw.cloud"
  pg_backups_s3_access_key    = var.scaleway_access_key
  pg_backups_s3_secret_key    = var.scaleway_secret_key
  pg_backups_s3_full_schedule = "45 4 * * 0"
  pg_backups_s3_incr_schedule = "45 4 * * 1-6"
  values = [
    file("${path.module}/db_resources.yaml"),
    <<-EOT
      imagePgBackRest: null
      imagePostgres: null
      postgresVersion: 14
    EOT
  ]
}

resource "helm_release" "metabase" {
  repository = "https://gitlab.com/api/v4/projects/29298483/packages/helm/stable"
  chart      = "nodejs"
  name       = "metabase"
  namespace  = module.namespace.namespace
  values = [
    <<-EOT
    image:
      repository: metabase/metabase
      tag: v0.46.6.4
    envVars:
      MB_DB_TYPE: postgres
      MB_DB_CONNECTION_URI:
        secretKeyRef:
          name: postgresql-pguser-postgresql
          key: jdbc-uri
      MB_EMAIL_SMTP_HOST: ${var.smtp_host}
      MB_EMAIL_SMTP_PORT: ${var.smtp_port}
      MB_EMAIL_SMTP_USERNAME: ${var.smtp_user}
      MB_EMAIL_SMTP_PASSWORD: ${var.smtp_password}
      MB_EMAIL_SMTP_SECURITY: ${var.smtp_security}
      MB_EMAIL_FROM_ADDRESS: ${var.smtp_from_address}
      MB_EMAIL_FROM_NAME: ${var.smtp_from_name}
    service:
      targetPort: 3000
    probes:
      liveness:
        tcpSocket:
          port: 3000
      readiness:
        tcpSocket:
          port: 3000
    ingress:
      annotations:
        kubernetes.io/ingress.class: haproxy
      enabled: false
      host: ${var.hostname}
    resources:
      limits:
        memory: 2048Mi
      requests:
        cpu: 500m
    EOT
    , <<-EOT
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
    EOT
  ]
}
